# -*- coding: utf-8 -*-

"""
--------------------------------------------------------------------------------
Author:		Fons Laan, ILPS-ISLA, University of Amsterdam
Project:	BiLand
Name:		timestamp.py
Version:	0.1
Goal:		server timestamp for comparison with client timestamp

FL-19-Jun-2013: Created
FL-29-Jan-2013: Changed
"""

TIMESTAMP = "29-Jan-2014 15:17"		# must be identical in timestamp.js

# [eof]
